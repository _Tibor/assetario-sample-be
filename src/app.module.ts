import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CitiesModule } from './cities/cities.module';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }),
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                uri: configService.get('db.uri'),
                dbName: configService.get('db.name'),
                useNewUrlParser: true
            }),
            inject: [ConfigService]
        }),
        CitiesModule],
    controllers: [AppController],
    providers: [AppService, ConfigService],
})
export class AppModule { }