import { Controller, Get } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
@Controller()
export class AppController {
    constructor(
        private readonly configService: ConfigService) { }

    @Get()
    getInfo(): any {
        return {
            "appName": this.configService.get<string>("app.name"),
            "version": this.configService.get<string>("app.version")
        };
    }
}
