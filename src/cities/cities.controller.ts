import { Body, Controller, Delete, Get, Param, Post, Query, Req, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CitiesService } from './cities.service';

@Controller('cities')
export class CitiesController {
    constructor(private readonly service: CitiesService) {
    }

    @Get()
    async index() {
        return await this.service.findAll();
    }

    @Get('search')
    async search(@Query('term') term) {
        return await this.service.searchCities(term);
    }

    @Get(':id')
    async find(@Param('id') id: string) {
        return await this.service.findOne(id);
    }

    @Post()
    @UseInterceptors(FileInterceptor('cities'))
    async uploadCities(@UploadedFile() cities: Express.Multer.File) {
        await this.service.parseAndSaveCities(cities.buffer);
        console.log("upload cities finished");
        return {"result": "ok"};
    }

    @Delete('id')
    async delete(@Param('id') id: string) {
        return await this.service.delete(id);
    }
}
