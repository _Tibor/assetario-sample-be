export class CityDto {
    city_id: number;
    city: string;
    city_ascii: string;
    state_id: string;
    state_name: string;
    county_fips: number;
    county_name: string;
    lat: number;
    lng: number;
    population: number;
    density: number;
    source: string;
    military: boolean;
    incorporated: boolean;
    timezone: string;
    ranking: number;

    constructor(csvData: any = null) {
        if (csvData) {
            this.parseCsvLine(csvData);
        }
    }

    private parseCsvLine(csvData: any) {
        this.city = csvData.city;
        this.city_id = Number.parseInt(csvData.id);
        this.city_ascii = csvData.city_ascii;
        this.state_id = csvData.state_id;
        this.state_name = csvData.state_name;
        this.county_fips = Number.parseInt(csvData.county_fips);
        this.county_name = csvData.county_name;
        this.lat = Number.parseFloat(csvData.lat);
        this.lng = Number.parseFloat(csvData.lng);
        this.population = Number.parseInt(csvData.population);
        this.density = Number.parseInt(csvData.density);
        this.source = csvData.source;
        this.military = csvData.military.toLowerCase() === 'true';
        this.incorporated = csvData.incorporated.toLowerCase() === 'true';
        this.timezone = csvData.timezone;
        this.ranking = Number.parseInt(csvData.ranking);
    }
}