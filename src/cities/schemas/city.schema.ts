import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CityDocument = City & Document;

@Schema()
export class City {

    @Prop({ required: true })
    city_id: number;

    @Prop({ required: true })
    city: string;

    @Prop({ required: true })
    city_ascii: string;

    @Prop({ required: true })
    state_id: string;

    @Prop({ required: true })
    state_name: string;

    @Prop({ required: true })
    county_fips: number;

    @Prop({ required: true })
    county_name: string;

    @Prop({ required: true })
    lat: number;

    @Prop({ required: true })
    lng: number;

    @Prop({ required: true })
    population: number;

    @Prop({ required: true })
    density: number;

    @Prop({ required: true })
    source: string;

    @Prop({ required: true })
    military: boolean;

    @Prop({ required: true })
    incorporated: boolean;

    @Prop({ required: true })
    timezone: string;

    @Prop({ required: true })
    ranking: number;
}

const CitySchema = SchemaFactory.createForClass(City);
CitySchema.index({ city: 'text' });

export { CitySchema };