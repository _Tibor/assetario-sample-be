import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CityDto } from './dto/city.dto';
import { City, CityDocument } from './schemas/city.schema';
import { parse } from 'csv-parse';
import { DeleteResult } from 'mongodb';
@Injectable()
export class CitiesService {
    constructor(@InjectModel(City.name) private readonly model: Model<CityDocument>) {
    }

    async findAll(): Promise<City[]> {
        return await this.model.find().exec();
    }

    async findOne(id: string): Promise<City> {
        return await this.model.findById(id).exec();
    }

    async create(createTodoDto: CityDto): Promise<City> {
        return await new this.model({ ...createTodoDto, createdAt: new Date() }).save();
    }

    async delete(id: string): Promise<City> {
        return await this.model.findByIdAndDelete(id).exec();
    }

    async deleteAll(): Promise<DeleteResult> {
        return await this.model.deleteMany().exec();
    }

    async parseAndSaveCities(buffer: Buffer): Promise<void> {

        await this.deleteAll();

        return new Promise((resolve, reject) => {

            console.log("Start CSV parser");

            const cities = new Array<CityDto>();

            const parser = parse(buffer, {
                delimiter: ',',
                columns: true,
                cast: true
            });

            parser.on('readable', () => {
                let record;
                while ((record = parser.read()) !== null) {
                    cities.push(new CityDto(record));
                }
            });
            parser.on('end', () => {
                // save to DB
                if (cities.length > 0) {
                    this.model.insertMany(cities).then(() => { resolve(); } );
                }
                else {
                    resolve();
                }
            });
            parser.on('error', () => { reject() });
        });
    }

    async searchCities(searchTerm: string) : Promise<any> {
        const cities = await this.model.find( { "city" : { "$regex": searchTerm, "$options": "i" } } );
        return this.createCitiesResponse(cities, true);
    }

    private createCitiesResponse(cities: City[], arrayResponse: boolean = false) : any {
        let result = {};
        result['count'] = cities.length;

        const startTime = (new Date()).getTime();

        cities.forEach(city => {
            if (!result[city.state_id]) {
                result[city.state_id] = {};
            }
            if (!result[city.state_id][city.county_name]) {
                result[city.state_id][city.county_name] = [];
            }

            result[city.state_id][city.county_name].push(city);
        });

        if (arrayResponse) {
            result = this.createCitiesArrayResonse(result);
        }

        const endTime = (new Date()).getTime();

        console.log(`Duration: ${endTime - startTime} ms`);

        return result;
    }

    private createCitiesArrayResonse(data: any): any {

        const result = {
            "count": data.count,
            "cities": []
        };

        Object.keys(data).forEach(stateKey => {
            if (stateKey == "count") {
                return;
            }

            const state = { };
            state[stateKey] = [];
            Object.keys(data[stateKey]).forEach(countryKey => {

                const country = { };
                country[countryKey] = data[stateKey][countryKey];

                state[stateKey].push(country);
            });

            result.cities.push(state);
        });

        return result;
    }
}
